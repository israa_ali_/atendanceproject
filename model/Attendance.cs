﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherAttendance.model
{
    class Attendance
    {

        private int teacherId;
        private int courseId;
        private int roomId;
        private String startTime;
        private String leaveTime;
        private String comment;

        private String roomname;
        public Attendance(int teacherId, int courseId, int roomId, String startTime, String leaveTime, String comment)
        {
            this.teacherId = teacherId;
            this.courseId = courseId;
            this.roomId = roomId;
            this.startTime = startTime;
            this.leaveTime = leaveTime;
            this.comment = comment;
        }


        public int TeacherId {
            set
            {
                teacherId = value;
            }
            get
            {
                return this.teacherId;
            }
           }
        public int CourseId {
            set
            {
                courseId = value;
            }
            get
            {
                return this.courseId;
            }
          }
        public int RoomId
        {
            set
            {
                roomId = value;
            }
            get
            {
                return this.roomId;
            }
        }
        public String StartTime
        {
            set
            {
                startTime = value;
            }
            get
            {
                return this.startTime;
            }
        }
        public String LeaveTime
        {
            set
            {
                leaveTime = value;
            }
            get
            {
                return this.leaveTime;
            }
        }
        public String Comment
        {
            set
            {
                comment = value;
            }
            get
            {
                return this.comment;
            }
        }

    }
}
